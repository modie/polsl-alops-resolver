#ifndef POLSL_ALOPS_RESOLVER_LIST_H
#define POLSL_ALOPS_RESOLVER_LIST_H
#include <iostream>

template<typename Type>
struct Element{
    Type value;
    Element* pNext;
    Element* pPrevious;
};

template  <class T>
class List
{
private:
    Element<T>* pHead;
    Element<T>* pTail;
    unsigned int size;
public:
    List<T>(){
        pHead = nullptr;
        pTail = nullptr;
        size = 0;
    }
    List<T>(const List<T> &other);
    ~List<T>() {
        clear();
    }
    Element<T>* getHead();
    Element<T>* getTail();
    unsigned int const getSize();

    bool push_front( const T& value );
    bool push_back( const T& value );
    bool erase( Element<T>* pElement );
    void printIteratively(std::ostream & ss);
    void rPrintItearively(std::ostream & ss);
    bool clear();
};

#endif //POLSL_ALOPS_RESOLVER_LIST_H
