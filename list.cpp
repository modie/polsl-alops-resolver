#include <iostream>
#include "list.h"

template<class T>
List<T>::List(const List<T> &other) {
    pHead = nullptr;
    pTail = nullptr;
    size = 0;
    Element<T> *otherHead = other.pHead;
    while (otherHead) {
        push_back(otherHead->value);
        otherHead = otherHead->pNext;
    }
}

template<class T>
Element<T> *List<T>::getHead() {
    return List<T>::pHead;
}

template<class T>
Element<T> *List<T>::getTail() {
    return List<T>::pTail;
}

template<class T>
unsigned int const List<T>::getSize() {
    return List<T>::size;
}

template<class T>
bool List<T>::push_front(const T &value) {
    auto *pNew = new Element<T>{value, pHead, nullptr};
    if (pHead) {
        pHead->pPrevious = pNew;
    } else {
        pTail = pNew;
    }
    pHead = pNew;
    return true;
}

template<class T>
bool List<T>::push_back(const T &value) {
    auto *pNew = new Element<T>{value, nullptr, pTail};
    if (pTail) {
        pTail->pNext = pNew;
    } else {
        pHead = pNew;
    }
    pTail = pNew;
    return true;
}

template<class T>
bool List<T>::erase(Element<T> *pElement) {
    pElement->pPrevious->pNext = pElement->pNext;
    pElement->pNext->pPrevious = pElement->pPrevious;
    delete pElement;
}

template<class T>
void List<T>::printIteratively(std::ostream &ss) {
    auto ptr = List<T>::pHead;
    while (ptr) {
        ss << ptr->value;
        ptr = ptr->pNext;
    }
}

template<class T>
void List<T>::rPrintItearively(std::ostream &ss) {
    auto ptr = List<T>::pTail;
    while (ptr) {
        ss << ptr->value;
        ptr = ptr->pTail;
    }
}

template<class T>
bool List<T>::clear(){
    auto ptr = List<T>::pHead;
    while (ptr) {
        Element<T> *pNext = ptr->pNext;
        delete ptr;
        ptr = pNext;
    }
    List<T>::pHead = nullptr;
    List<T>::pTail = nullptr;
    return true;
}






